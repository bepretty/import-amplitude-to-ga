# import-amplitude-to-ga

Node.js script to import Order Completed and Successful Booking events from Amplitude to Google Analytics.

## Setup data files

Run the `normalizationLinux.sh` (or `normalizationOSx.sh` if on OSx) in the same directory as the `json.gzip` files.

## Verify data files

`python3 validator.py` will output the number of files read and the number of files with errors.
If the second output is 0 all is ok.

## Setup import script

`npm install`

## Run

The script will read all `.json` files (except the npm package files) found in the project root directory and import the events to GA.

**You'll need to replace `ACCOUNT_ID` to match the Google Analytics account to where you want to import events.**

`node --max_old_space_size=2048 import.js -a ACCOUNT_ID`
