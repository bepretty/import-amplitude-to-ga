from os import walk
import json

def main():
    files = []
    for (dirpath, dirnames, filenames) in walk('.'):
        files.extend(filenames)
        break

    error_files = []

    for f in filter(lambda fn: fn.endswith('.json'), files):
        with open(f) as fp:
            try:
                json.load(fp)
            except ValueError as e:
                print(e)
                print(f)
                error_files.append(f)

    print(len(files))
    print(len(error_files))

if __name__ == "__main__":
    main()

