const CATEGORY_ALL = "All",
      SUCCESSFUL_BOOKING_ACTION = "Successful Booking",
      ORDER_COMPLETED = "Order Completed";

var ACCOUNT_ID = "";
var THROTTLE_INTERVAL = 1000;
var THROTTLE_AMOUNT = 1000;
var THROTTLE = false;
var DRY_RUN = false;

const ua = require("universal-analytics");
const fs = require("fs");
const arg = require('arg');
const sleep = require('sleep');
const util = require('util');

const args = arg({
  '-a': String,
  '-t': Boolean,
  '-ti': Number,
  '-ta': Number,
  '-dry': Boolean
});

if ('-a' in args) {
  ACCOUNT_ID = args['-a'];
}

if ('-ti' in args) {
  THROTTLE_INTERVAL = args['-ti'];
}

if ('-ta' in args) {
  THROTTLE_AMOUNT = args['-ta'];
}

if ('-t' in args) {
  THROTTLE = true;
}

if ('-dry' in args) {
  DRY_RUN = true;
}

if (ACCOUNT_ID === "") throw new Error("No Google Analytics Account ID specified.");

console.log("Using Google Analytics Account ID: " + ACCOUNT_ID);
var visitor = ua(ACCOUNT_ID);

var getDocumentPathFromSuccessfulBookingEvent = function(amplitudeEvent) {
  eventProperties = amplitudeEvent.event_properties
  venueId = eventProperties['Venue Id'];
  serviceId = eventProperties['Service Id'];
  documentPath = null;
  if (venueId && serviceId) {
    documentPath = "/venues/" + venueId + "/services/" + serviceId + "/booking";
  }
  return documentPath;
};

var getDocumentPathFromOrderCompletedEvent = function(amplitudeEvent) {
  eventProperties = amplitudeEvent.event_properties;
  venueId = eventProperties['booking.venue_id'];
  serviceId = eventProperties['booking.service_id'];
  documentPath = null;
  if (venueId && serviceId) {
    documentPath = "/venues/" + venueId + "/services/" + serviceId + "/booking";
  }
  return documentPath;
};

var importAmplitudeEvent = function(amplitudeEvent, documentPath) {
  userProperties = amplitudeEvent.user_properties;
  var eventParams = {
    ec: CATEGORY_ALL,
    ea: amplitudeEvent.event_type,
    uid: userProperties.id,
    cn: userProperties.utm_campaign,
    cc: userProperties.utm_content,
    cm: userProperties.utm_medium,
    cs: userProperties.utm_source,
    ck: userProperties.utm_term,
    dr: userProperties.referrer,
    dp: documentPath
  }
  visitor.event(eventParams, function(err) {
    if (err) {
      console.error(err);
      console.error(
        "Error sending event with payload: " +
        util.inspect(eventParams, false, null)
      );
    }
  });
};

var getItemFromEvent = function(amplitudeEvent) {
  eventProperties = amplitudeEvent.event_properties;
  return {
    in: eventProperties['booking.service_name'],
    ip: eventProperties['booking.final_price'],
    cu: eventProperties.currency,
    iq: 1,
    ti: eventProperties.order_id
  };
};

var getTransactionFromEvent = function(amplitudeEvent) {
  eventProperties = amplitudeEvent.event_properties;
  return {
    ti: eventProperties.order_id,
    cu: eventProperties.currency,
    ta: eventProperties.affiliation,
    tr: eventProperties.total
  };
};

var trackSuccessfulBooking = function(amplitudeEvent) {
  importAmplitudeEvent(
    amplitudeEvent,
    getDocumentPathFromSuccessfulBookingEvent(amplitudeEvent)
  )
};

var trackOrderCompleted = function(amplitudeEvent) {
  var itemParams = getItemFromEvent(amplitudeEvent);
  var transactionParams = getTransactionFromEvent(amplitudeEvent);
  visitor.transaction(transactionParams, function(err) {
    if (err) {
      console.error(err);
      console.error(
        "Error sending transaction with payload: " +
        util.inspect(transactionParams, false, null)
      );
    }
  }).item(itemParams, function(err) {
    if (err) {
      console.error(err);
      console.error(
        "Error sending item with payload " +
        util.inspect(itemParams, false, null)
      );
    }
  });
  importAmplitudeEvent(
    amplitudeEvent,
    getDocumentPathFromOrderCompletedEvent(amplitudeEvent)
  );
};

var amplitudeEvents = [];

console.log("Loading amplitude events.");

fs.readdirSync(".").forEach(file => {
  if (file.endsWith(".json") && !file.startsWith("package")) {
    data = fs.readFileSync(file, "utf8");
    amplitudeEvents = amplitudeEvents.concat(JSON.parse(data));
  }
});

console.log("Done loading " + amplitudeEvents.length + " amplitude events.");

console.log("Importing amplitude events to Google Analytics");

var countEvents = 0;
var countSuccessfulBookings = 0;
var countOrderCompleted = 0;
var duplicateOrders = 0;
var orderIds = [];

amplitudeEvents.forEach(amplitudeEvent => {
  visitor = ua(ACCOUNT_ID);
  if ('user_properties' in amplitudeEvent) {
    userProperties = amplitudeEvent.user_properties;
    if ('version' in userProperties && userProperties.version.includes('react')) {
      if (amplitudeEvent.event_type === SUCCESSFUL_BOOKING_ACTION) {
        countSuccessfulBookings++;
        if (!DRY_RUN) {
          trackSuccessfulBooking(amplitudeEvent);
        }
        countEvents++;
        if (THROTTLE && countEvents === THROTTLE_AMOUNT) {
          countEvents = 0;
          console.log(
            "Sleeping for " + THROTTLE_INTERVAL + "ms after " +
            THROTTLE_AMOUNT + " events sent"
          );
          sleep.msleep(THROTTLE_INTERVAL);
        }
      } else if(amplitudeEvent.event_type === ORDER_COMPLETED) {
        countOrderCompleted++;
        eventProperties = amplitudeEvent.event_properties;
        if (orderIds.includes(eventProperties.order_id)) {
          duplicateOrders++;
        } else {
          orderIds = orderIds.concat([eventProperties.order_id]);
        }
        if (!DRY_RUN) {
          trackOrderCompleted(amplitudeEvent);
        }
        countEvents++;
        if (THROTTLE && countEvents === THROTTLE_AMOUNT) {
          countEvents = 0;
          console.log(
            "Sleeping for " + THROTTLE_INTERVAL + "ms after " +
            THROTTLE_AMOUNT + " events sent"
          );
          sleep.msleep(THROTTLE_INTERVAL);
        }
      }
    }
  }
});

console.log(
  "Imported " + countSuccessfulBookings +
  " events of type '" + SUCCESSFUL_BOOKING_ACTION + "'."
);

console.log(
  "Imported " + countOrderCompleted +
  " events of type '" + ORDER_COMPLETED + "'."
);

console.log("Found " + duplicateOrders + " duplicate orders.");

console.log("Done importing events.");
